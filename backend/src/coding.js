const { randomBytes, createDecipheriv, createCipheriv } = require('crypto');
const algorithm = 'aes-256-ctr';

let key = "very very secret key         ..."

const iv = randomBytes(16);

function decrypt(token) {
    try {
        let hash = token.split('_');
        if (hash.length < 2) return;

        const decipher = createDecipheriv(algorithm, key, Buffer.from(hash[0], 'hex'));
        const decrypted = Buffer.concat([decipher.update(Buffer.from(hash[1], 'hex')), decipher.final()]);
        return decrypted;
    } catch { /* illegal content or iv values */
        return;
    }
}

function encrypt(text) {
    const cipher = createCipheriv(algorithm, key, iv);
    const encrypted = Buffer.concat([cipher.update(text), cipher.final()]);
    return iv.toString('hex') + '_' + encrypted.toString('hex');
}

module.exports.encrypt = encrypt;
module.exports.decrypt = decrypt;