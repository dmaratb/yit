const express = require('express');
const cors = require('cors');
const routes = require('./routes');
const app = express();
const port = 3000;

app.use(cors({ credentials: true, origin: (_origin, callback) => { callback(null, true) } }));
app.use(express.json());
app.use('/', routes);

app.listen(port, () => {
    console.log('listening on port:' + port)
});