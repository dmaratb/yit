const { DB } = require('./db');


const AlbumLogic = {
    add: (req, callback) => {
        let userAlbums = DB.addAlbum(req.username, req.body.album);
        callback(200, { albums: userAlbums });
    },

    edit: (req, callback) => {
        let userAlbums = DB.editAlbum(req.username, req.body.album);
        callback(200, { albums: userAlbums });
    },

    delete: (req, callback) => {
        let userAlbums = DB.deleteAlbum(req.username, req.body.album);
        callback(200, { albums: userAlbums });
    },

    getAll: (req, callback) => {
        let userAlbums = DB.getAlbums(req.username);
        callback(200, { albums: userAlbums });
    }
}

module.exports.AlbumLogic = AlbumLogic;