const users = {};

/**
 * data base emulation, to be replaced with actual DAL
*/
const DB = {
    addUser: (username, password) => {
        users[username] = { password: password };
    },

    getUser: (username) => {
        return users[username];
    },

    addAlbum: (username, album) => {
        let allAlbums = users[username].albums;

        if (allAlbums) {
            album.id = allAlbums[allAlbums.length - 1].id + 1;
            users[username].albums.push(album);
        } else {
            album.id = 0;
            users[username].albums = [album];
        }

        return users[username].albums;
    },

    editAlbum: (username, album) => {
        if (users[username].albums) {
            const index = users[username].albums.findIndex(a => a.id == album.id);
            if (index > -1) {
                users[username].albums[index] = album;
            }
        }

        return users[username].albums;
    },

    deleteAlbum: (username, album) => {
        if (users[username].albums) {
            const index = users[username].albums.findIndex(a => a.id == album.id)
            if (index > -1) {
                users[username].albums.splice(index, 1);
            }
        }

        return users[username].albums;
    },

    getAlbums: (username) => {
        return users[username].albums;
    }
}

module.exports.DB = DB;