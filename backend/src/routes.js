const router = require('express').Router();
const { auth } = require('./auth');
const { AlbumLogic } = require('./album.logic');
const { UserLogic } = require('./user.logic');


router.post('/register', function (req, res) {
    UserLogic.register(req, (status, data) => {
        res.status(status);
        res.send(data);
    });
});

router.post('/login', function (req, res) {
    UserLogic.login(req, (status, data) => {
        res.status(status);
        res.send(data);
    });
});

/**
 * get all user's albums
*/
router.get('/albums', auth(), function (req, res) {
    AlbumLogic.getAll(req, (status, data) => {
        res.status(status);
        res.send(data);
    });
});

/**
 * add album
*/
router.put('/album', auth(), function (req, res) {
    AlbumLogic.add(req, (status, data) => {
        res.status(status);
        res.send(data);
    });
});

/**
 * edit album
*/
router.post('/album', auth(), function (req, res) {
    AlbumLogic.edit(req, (status, data) => {
        res.status(status);
        res.send(data);
    });
});

/**
 * delete album
*/
router.post('/delete', auth(), function (req, res) {
    AlbumLogic.delete(req, (status, data) => {
        res.status(status);
        res.send(data);
    });
});

module.exports = router;