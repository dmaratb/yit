const { encrypt } = require('./coding');
const { DB } = require('./db');

const UserLogic = {
    register: (req, callback) => {
        const user = DB.getUser(req.body.username);

        if (user) {
            callback(400);
            return;
        }

        DB.addUser(req.body.username, req.body.password);
        callback(200, { token: encrypt(req.body.username), username: req.body.username });
    },

    login: (req, callback) => {
        const user = DB.getUser(req.body.username);

        if (!user) {
            callback(400);
            return;
        }

        if (user.password != req.body.password) {
            callback(400);
            return;
        }

        callback(200, { token: encrypt(req.body.username), username: req.body.username });
    }
}

module.exports.UserLogic = UserLogic;