const { DB } = require('./db');
const { decrypt } = require('./coding');


function auth() {

    return async (req, res, next) => {
        const token = req.headers["x-access-token"] || req.headers["authorization"];
        if (!token) {
            res.status(401).json({ error: 'Invalid request' });
            return;
        }

        let username = decrypt(token);
        if (!username) {
            res.status(401).json({ error: 'Login again' });
            return;
        }

        let user;
        try {
            user = DB.getUser(username);
        } catch (err) {
            res.status(500).json({ error: 'Fetch failure' });
            return;
        }

        if (user) {
            req.username = username;
            next();
        } else {
            res.status(403).json({ error: 'Forbidden' });
        }
    }
}

module.exports.auth = auth;