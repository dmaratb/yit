import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { WebService } from 'src/app/services/web.service';

@Component({
  selector: 'register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css'],
})
export class RegisterComponent implements OnInit {
  registerForm = this.formBuilder.group({
    username: ['', [Validators.required]],
    password: ['', [Validators.required]],
  });

  constructor(
    private formBuilder: FormBuilder,
    private webService: WebService,
    private router: Router,
    private alertPopup: MatSnackBar
  ) {}

  ngOnInit() {}

  submit() {
    if (this.registerForm.invalid) {
      return;
    }

    this.webService
      .register(
        this.registerForm.controls.username.value,
        this.registerForm.controls.password.value
      )
      .then(
        (response) => {
          this.webService.updateCredentials(response);
          this.router.navigate(['/']);
        },
        (error) => {
          this.alertPopup.open('username already exists', undefined, {
            duration: 500,
            horizontalPosition: 'center',
            verticalPosition: 'top',
            panelClass: 'error-popup',
          });
        }
      );
  }
}
