import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { WebService } from 'src/app/services/web.service';

@Component({
  selector: 'login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {
  loginForm = this.formBuilder.group({
    username: ['', [Validators.required]],
    password: ['', [Validators.required]],
  });

  constructor(
    private formBuilder: FormBuilder,
    private webService: WebService,
    private router: Router,
    private alertPopup: MatSnackBar
  ) {}

  ngOnInit() {}

  submit() {
    if (this.loginForm.invalid) {
      return;
    }

    this.webService
      .login(
        this.loginForm.controls.username.value,
        this.loginForm.controls.password.value
      )
      .then(
        (response) => {
          this.webService.updateCredentials(response);
          this.router.navigate(['/']);
        },
        (error) => {
          this.alertPopup.open('incorrect username or password', undefined, {
            duration: 500,
            horizontalPosition: 'center',
            verticalPosition: 'top',
            panelClass: 'error-popup',
          });
        }
      );
  }
}
