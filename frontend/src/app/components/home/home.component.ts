import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import {
  MatDialog,
  MatDialogRef,
  MAT_DIALOG_DATA,
} from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { Album } from 'src/app/models/album.model';
import { WebService } from 'src/app/services/web.service';

export interface AlbumDialogMode {
  mode: 'add' | 'edit';
  album: Album;
}

@Component({
  selector: 'home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
})
export class HomeComponent implements OnInit {
  username = '';
  orderedBy = '';
  albums: Album[] = [];
  years: { value: number; checked: boolean }[] = [];
  artists: { value: string; checked: boolean }[] = [];

  constructor(
    private webService: WebService,
    public dialog: MatDialog,
    private alertPopup: MatSnackBar,
    private router: Router
  ) {}

  ngOnInit() {
    this.username = this.webService.username;

    this.webService.getAlbums().then(
      (response) => {
        this.setAlbums(response.albums);
      },
      (error) => {
        this.handleError(error);
      }
    );
  }

  reset(field: string) {
    switch (field) {
      case 'years':
        this.years.forEach((year) => (year.checked = true));
        break;
      case 'artists':
        this.artists.forEach((artist) => (artist.checked = true));
        break;
      default:
        break;
    }
  }

  order(field: string) {
    switch (field) {
      case 'years':
        this.albums.sort((a, b) => {
          return a.year - b.year;
        });
        break;
      case 'artists':
        this.albums.sort((a, b) => {
          return a.artist > b.artist ? 1 : -1;
        });
        break;
      default:
        break;
    }
  }

  addAlbum() {
    this.dialog
      .open(AddAlbumDialog, { data: { mode: 'add' } })
      .afterClosed()
      .subscribe((album) => {
        if (album) {
          this.webService.addAlbum(album).then(
            (response) => {
              this.setAlbums(response.albums);
            },
            (error) => {
              this.handleError(error);
            }
          );
        }
      });
  }

  editAlbum(album: Album) {
    this.dialog
      .open(AddAlbumDialog, { data: { mode: 'edit', album: album } })
      .afterClosed()
      .subscribe((album) => {
        if (album) {
          this.webService.editAlbum(album).then(
            (response) => {
              this.setAlbums(response.albums);
            },
            (error) => {
              this.handleError(error);
            }
          );
        }
      });
  }

  deleteAlbum(album: Album) {
    this.webService.deleteAlbum(album).then(
      (response) => {
        this.setAlbums(response.albums);
      },
      (error) => {
        this.handleError(error);
      }
    );
  }

  logout() {
    this.webService.logout();
    this.router.navigate(['/login']);
  }

  setAlbums(albums: Album[] = []) {
    this.albums = albums;

    let allArtists = [...new Set(albums.map((album) => album.artist))];
    this.artists = allArtists.map((value) => {
      let oldArtist = this.artists.find((artist) => artist.value == value);
      let artist = {
        value: value,
        checked: oldArtist ? oldArtist.checked : true,
      };
      return artist;
    });

    let allYears = [...new Set(albums.map((album) => album.year))];
    this.years = allYears.map((value) => {
      let oldYear = this.years.find((year) => year.value == value);
      let year = {
        value: value,
        checked: oldYear ? oldYear.checked : true,
      };
      return year;
    });

    if (this.orderedBy.length > 0) {
      this.order(this.orderedBy);
    }
  }

  handleError(error: { status: number }) {
    if (error.status == 401 || error.status == 403) {
      this.router.navigate(['/login']);
    } else {
      this.alertPopup.open('error occured', undefined, {
        duration: 500,
        horizontalPosition: 'center',
        verticalPosition: 'top',
        panelClass: 'error-popup',
      });
    }
  }

  get filteredAlbums() {
    return this.albums.filter((album) => {
      let year = this.years.find((year) => year.value == album.year);
      let artist = this.artists.find((artist) => artist.value == album.artist);
      if (year?.checked && artist?.checked) return true;
      return false;
    });
  }
}

@Component({
  selector: 'album-dialog',
  templateUrl: 'album.dialog.html',
})
export class AddAlbumDialog {
  id = -1;
  albumForm = this.formBuilder.group({
    title: ['', [Validators.required]],
    artist: ['', [Validators.required]],
    year: ['', [Validators.required]],
    genre: ['', [Validators.required]],
  });

  constructor(
    public formBuilder: FormBuilder,
    public albumDialog: MatDialogRef<AddAlbumDialog>,
    @Inject(MAT_DIALOG_DATA) public data: AlbumDialogMode
  ) {
    albumDialog.afterOpened().subscribe(() => {
      if (data.mode == 'edit') {
        this.id = data.album.id;
        this.albumForm = this.formBuilder.group({
          title: [data.album.title, [Validators.required]],
          artist: [data.album.artist, [Validators.required]],
          year: [data.album.year, [Validators.required]],
          genre: [data.album.genre, [Validators.required]],
        });
      }
    });
  }

  save() {
    if (this.albumForm.invalid) {
      return;
    }

    this.albumDialog.close({
      id: this.id,
      title: this.albumForm.controls.title.value,
      artist: this.albumForm.controls.artist.value,
      year: this.albumForm.controls.year.value,
      genre: this.albumForm.controls.genre.value,
    });
  }

  cancel() {
    this.albumDialog.close();
  }
}
