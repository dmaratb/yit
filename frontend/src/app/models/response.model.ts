import { Album } from 'src/app/models/album.model';

export interface LoginResponse {
  username: string;
  token: string;
}

export interface AlbumsResponse {
  albums: Album[];
}
