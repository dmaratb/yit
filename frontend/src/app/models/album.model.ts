export interface Album {
  id: number;
  title: string;
  year: number;
  artist: string;
  genre: string;
}
