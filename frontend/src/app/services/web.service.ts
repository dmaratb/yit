import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { Album } from '../models/album.model';
import { AlbumsResponse, LoginResponse } from './../models/response.model';

@Injectable({ providedIn: 'root' })
export class WebService {
  baseUrl = 'http://localhost:3000/';
  token = '';
  username = '';
  headers: HttpHeaders | { [header: string]: string | string[] } | undefined;

  constructor(private http: HttpClient, private cookieService: CookieService) {
    this.username = cookieService.get('username');
    this.token = cookieService.get('token');
    this.headers = new HttpHeaders({ authorization: this.token });
  }

  updateCredentials(credentials: LoginResponse) {
    this.username = credentials.username;
    this.token = credentials.token;
    this.cookieService.set('token', credentials.token);
    this.cookieService.set('username', credentials.username);
    this.headers = new HttpHeaders({ authorization: credentials.token });
  }

  register(username: string, password: string) {
    return this.http
      .post<LoginResponse>(this.baseUrl + 'register', {
        username: username,
        password: password,
      })
      .toPromise();
  }

  login(username: string, password: string) {
    return this.http
      .post<LoginResponse>(this.baseUrl + 'login', {
        username: username,
        password: password,
      })
      .toPromise();
  }

  logout() {
    this.username = '';
    this.token = '';
    this.cookieService.delete('token');
    this.cookieService.delete('username');
    this.headers = new HttpHeaders();
  }

  getAlbums() {
    return this.http
      .get<AlbumsResponse>(this.baseUrl + 'albums', { headers: this.headers })
      .toPromise();
  }

  addAlbum(album: Album) {
    return this.http
      .put<AlbumsResponse>(
        this.baseUrl + 'album',
        { album: album },
        { headers: this.headers }
      )
      .toPromise();
  }

  editAlbum(album: Album) {
    return this.http
      .post<AlbumsResponse>(
        this.baseUrl + 'album',
        { album: album },
        { headers: this.headers }
      )
      .toPromise();
  }

  deleteAlbum(album: Album) {
    return this.http
      .post<AlbumsResponse>(
        this.baseUrl + 'delete',
        { album: album },
        { headers: this.headers }
      )
      .toPromise();
  }
}
