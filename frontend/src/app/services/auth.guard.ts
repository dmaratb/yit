import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { WebService } from './web.service';

@Injectable({ providedIn: 'root' })
export class AuthGuard implements CanActivate {
  constructor(private router: Router, private webService: WebService) {}

  canActivate() {
    const token = this.webService.token;
    if (token.length > 0) {
      return true;
    }

    this.router.navigate(['/login']);
    return false;
  }
}
